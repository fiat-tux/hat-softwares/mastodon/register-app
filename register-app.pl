#!/usr/bin/perl
use strict;
use warnings;
use 5.10.0;

use Mastodon::Client;

my $client = Mastodon::Client->new(
    instance => $ENV{HOST},
    name     => $ENV{APP},
    website  => 'https://framagit.org/fiat-tux/hat-softwares/mastodon/register-app',
    scopes   => ['write']
);

$client = $client->register();

say 'Please keep the client id and secret to be able to run the bot:';
say ' - Client id: ',     $client->client_id;
say ' - Client secret: ', $client->client_secret;
say 'Click on this URL to authorize the app to post on your account: ', $client->authorization_url();

say 'You will get an account token. Please, give it to me:';

my $access_code = <STDIN>;
chomp $access_code;
$client->authorize(
    access_code => $access_code
);

say 'This is the access_token, please keep it: ', $client->access_token;

say "\n", 'This is for your convenience:';
say 'export CLIENT_ID=',     $client->client_id;
say 'export CLIENT_SECRET=', $client->client_secret;
say 'export ACCESS_TOKEN=',  $client->access_token
