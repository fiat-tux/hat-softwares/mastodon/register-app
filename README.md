# Register app

This software allows you to register an application to [Mastodon](https://joinmastodon.org) and get credentials to use in your app.

This software uses [Fiat Tux Code of conduct](https://framagit.org/fiat-tux/code-of-conduct/blob/master/README.md).

## How to use?

First, get the software:

```
sudo apt install git build-essential liblwp-protocol-https-perl
git clone https://framagit.org/fiat-tux/hat-softwares/mastodon/register-app
cd register-app
```

Then register your app, considering that your Mastodon instance is `exemple.org`:

```
sudo cpan Carton
carton install --deployment
# The next comment fixes a bug in Mastodon::Client module
sed -r -e '/has scopes/ {n ; s/ro/rw/}' -i local/lib/perl5/Mastodon/Client.pm
APP="my-wonderful-app" HOST="exemple.org" carton exec ./register-app.pl
```

This will give you three secret environment variables: `CLIENT_ID`, `CLIENT_SECRET` and `ACCESS_TOKEN`.
Keep them well and secure!

Then use those secrets in your application.

## License

GPLv3, see the [LICENSE](LICENSE) file for details.

## Logo

The logo is the [mascot of Mastodon](https://commons.wikimedia.org/wiki/File%3AMastodon_Mascot_%28Alternative%29.png)

## Author

[Luc Didry](https://fiat-tux.fr). You can support me on [Tipeee](https://tipeee.com/fiat-tux) and [Liberapay](https://liberapay.com/sky).

[![Tipeee button](https://framagit.org/luc/last/raw/master/themes/default/img/tipeee-tip-btn.png)](https://tipeee.com/fiat-tux) [![Liberapay logo](https://framagit.org/luc/last/raw/master/themes/default/img/liberapay.png)](https://liberapay.com/sky)
